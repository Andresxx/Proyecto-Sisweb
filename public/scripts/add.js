
function addProduct(){

    const cantidad=document.getElementById("cantidad");
    const tabla=document.getElementById("t01");
    const selector = document.getElementById('selector');
    const selectorValue = selector[selector.selectedIndex].value;
    
    
    /*Crear Elementos*/
    var label = document.createElement("label");
    var labelNote = document.createTextNode(cantidad.value);
    label.appendChild(labelNote);
    label.setAttribute("name","cantidad")


    var button = document.createElement("button");
    var buttonNote = document.createTextNode("Eliminar");
    button.appendChild(buttonNote);
    button.addEventListener('click', () => row.remove());

    var produc = document.createElement("label");
    var producNote = document.createTextNode(selectorValue);
    produc.appendChild(producNote);
    produc.setAttribute("name","productos");
    /*----------------------------------------- */

    if(cantidad.value !== "0"){
        var row =tabla.insertRow(1);
        var cell1 = row.insertCell(0);
        var cell2 = row.insertCell(1);
        var cell3 = row.insertCell(2);        
        cell1.appendChild(produc);
        cell2.appendChild(label);
        cell3.appendChild(button);    
        
    
    }
    else{
        alert("Debe tener una cantidad distinta de cero para agregar una nueva fila");
    }
}