const express = require("express");
const app = express();
const bodyParser = require('body-parser');
const MongoClient = require('mongodb').MongoClient;

const DATABASE_NAME = 'productos';
const MONGO_URL =`mongodb://localhost:27017`;

let db = null;
let collection = null;
let pedidos=null;


app.set('view engine', 'ejs');
app.use(express.static("public"))
app.use(bodyParser.urlencoded({ extended: true }));



async function start(req, res) {
    const cursor1 = await pedidos.find().project({ cliente: 1 }).sort({ cliente: 1 });
    // const cursor2 = await pedidos.find().project({ numeroPedido: 1 }).sort({ numeroPedido: 1 });
    // const cursor3 = await pedidos.find().project({ fecha: 1 }).sort({ fecha: 1 });
    

    const array1 = await cursor1.toArray()
    const clientes = array1.map( o => o.cliente );
  

    // const array2 = await cursor2.toArray()
    // const np = array2.map( o => o.numeroPedido );
    
    // const array3 = await cursor3.toArray()
    // const fecha = array3.map( o => o.cliente );

    res.render("root", {clientes});
}




async function newproduct(req,res){

  const cursor = await collection.find()
                                   .project({ name: 1 })
                                   .sort({ name: 1 });
    const array = await cursor.toArray()
    const products = array.map( o => o.name );
    
    res.render("nuevoPedido", { products } );
}

var numeroPedido=1;
async function onSave(req,res){
  const cliente = req.body.cliente;
  const productos = req.body.productos;
  const cantidad = req.body.cantidad;
  // const f = new Date();
  // const fecha=f.getDate().toString() +'/'+d.getMonth().toString() +'/'+  d.getFullYear().toString() +' :'+ d.getHours().toString()+'hrs' ;

  const query = { cliente };
 const newEntry = { cliente, productos,cantidad,numeroPedido};
  const params = { upsert: true };
  const response = await pedidos.update(query, newEntry, params);
  numeroPedido++;
  res.render("root");  
}


app.get("/",start);
app.get("/nuevo",newproduct);
app.post("/save",onSave);



/* Iniciar server */
async function startServer() {
    const client = await MongoClient.connect(MONGO_URL, { useNewUrlParser: true });
    db = client.db(DATABASE_NAME);
    collection = db.collection('producto');
    pedidos=db.collection('pedidos');


    app.listen(3000);
    console.log("Server is up and running.\nOpen http://localhost:3000 to start");
}

startServer();
